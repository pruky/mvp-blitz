<?php
$start = microtime(True);
require '../app/User.php';
require '../app/WidgetRegister.php';
session_start();
function __autoload($className){
	if(strpos($className,'Presenter') != false){
		require_once('../app/presenters/'.$className.'.php');
	}
	elseif(strpos($className,'Model') != false) {
		require_once('../app/models/'.$className.'.php');
	}
	elseif(strpos($className,'Widget') != false) {
		require_once('../app/widgets/'.$className.'.php');
	}
	else{
		header('HTTP/1.0 404 Not Found');
	}
}
function createPage($data){
	$pageName = ucfirst($data['page']);
	unset($data['page']);
	$presenterName = $pageName.'Presenter';
	$presenter = new $presenterName($pageName,$data);
}
$array = array();
if(!isset($_GET['page'])){
	createPage(array('page'=>'homepage'));
	exit;	
}
$array = array_merge($_POST,$_GET);
createPage($array);
echo (microtime(True) - $start);


