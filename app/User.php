<?php
class User{
	public function register($data){
		$model = new UserModel();
		$model->insertUser($data);
	}
	public function logout(){
		session_unset();
		session_destroy();
	}
	public function login($username,$pass){
		session_regenerate_id();
		$model = new UserModel();
		$selection = $model->getPasswordAndId($username);
		list($hash, $id) = $selection;
		if (password_verify($pass,$hash)) {
			$_SESSION['id'] = $id;
			$_SESSION['username'] = $username;  
		}
	}
	public function isLoggedIn(){
		if(isset($_SESSION['id'])){
			return true;
		}
		return false;
	}
	public function getUserId(){
		if($this->isLoggedIn()){
			return $_SESSION['id'];
		}
		return null;
	}
	public function getUsername(){
		if($this->isLoggedIn()){
			return $_SESSION['username'];
		}
		return null;
	}
}