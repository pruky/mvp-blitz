<?php
class CorePresenter{
	protected $view;
	protected $model;
	protected $values = array();
	protected $user;
	public function __construct($pageName, $data = null){
		$this->user = new User();
		$model = $pageName.'Model';
		$this->model = new $model();	
		if(!isset($data['action'])){
			$tplFile = '../app/views/'.$pageName.'.tpl';
			$this->wannaRender($tplFile);
			$this->render($data);
			return;
		}
		$action = $data['action'].'Action';
		if(method_exists($this,$action)){
			unset($data['action']); 
			$this->$action($data);
		}
	}	
	public function wannaRender($tpl){
		$this->view = new Blitz($tpl);
	}
	public function redirectHome(){
		header('Location: ?page=homepage');
		exit;
	}
}