<?php
class UserModel extends CoreModel{
	public function getPasswordAndId($username){
		$selection = $this->query('SELECT password,id FROM user WHERE username = ?',true,array($username));
		return $selection;
	}
	public function insertUser($data){
		$this->insertArray('user',$data);
	}
}